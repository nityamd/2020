---
layout: post
title: Announcing PyGotham TV
date: 2020-06-12 21:39 -0400
---
PyGotham is going online-only for the first time ever.

In the interest of the health and safety of the PyGotham community, the
organizing team has decided not to hold an in-person conference this year. Stay
healthy, stay safe.

Instead, we’re thrilled to announce the first ever PyGotham TV, an online-only
edition of PyGotham. PyGotham TV will be a two-day, one-track conference and
completely free for attendees. This won’t just be a bunch videos, we’re creating
a show the likes of which you’ve never seen, and we look forward to sharing it
with the largest PyGotham audience ever. The call for proposals is live now
through July 5th [AoE](https://en.wikipedia.org/wiki/Anywhere_on_Earth) at
<https://cfp.pygotham.tv>. We encourage talk proposals from speakers of all
backgrounds about any topics that would be of interest to the Python community.
We can’t wait to see how our speakers make use of this new format. If you or
your company would like to support this event to keep PyGotham sustainable,
please see the sponsorship prospectus at
<https://2020.pygotham.tv/sponsors/prospectus/> and reach out to
[sponsors@pygotham.org](mailto:sponsors@pygotham.org). Sponsors are an integral
part of the PyGotham community, and we thank all of our returning and new
sponsors for supporting and participating in the conference.

Follow <https://2020.pygotham.tv> and <https://twitter.com/PyGotham> for more
information and announcements, and tune in on October 2, 2020 to be a part of a
brand new PyGotham experience.
