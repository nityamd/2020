---
name: Miroslav Šedivý
talks:
- "Your Name Is Invalid!"
---
Born in Czechoslovakia, studied in France, living in Germany. Languages
enthusiast and hjkl juggler. Using Python to get you the lowest prices
online. I like to discuss the human stuff in the IT: how humans write in
their languages, how they measure time and fiddle with time zones, and how
they can teach the computers to do the boring stuff for them.
