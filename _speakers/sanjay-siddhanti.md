---
name: Sanjay Siddhanti
talks:
- "Serverless Web Apps in Python"
---
I’m an Engineering Manager at Alpha Health. I love building software that
helps patients have a better experience with healthcare. My interests
include data engineering, web development, bioinformatics, and dev ops.

Previously I built software for laboratories that used DNA sequencing to
diagnose important health conditions.

I have a B.S. in Computer Science and M.S. in Biomedical Informatics from
Stanford University. Outside of software, I enjoy sports, cooking,
meditation, and reading.
