---
name: Hayley Denbraver
talks:
- "The Detective Has Arrived"
---
Hayley Denbraver has a background in both web development and developer
advocacy. Her specific interests include software security, engineering
ethics, and giving creative conference talks. She lives in Seattle and
enjoys taking her Labrador on hikes.
