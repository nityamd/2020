---
name: Jay Miller
talks:
- "Getting Better Transcriptions (for Developers)"
---
Jay is a Developer Advocate for Elastic and Podcaster. When not speaking or
recording, he can be found assisting developers in creating audio and visual
learning resources.
