---
name: Bruno Gonçalves
talks:
- "Visualization with seaborn"
---
Bruno Gonçalves is currently a Senior Data Scientist working at the
intersection of Data Science and Finance. Previously, he was a Data Science
fellow at NYU's Center for Data Science while on leave from a tenured
faculty position at Aix-Marseille Université. Since completing his PhD in
the Physics of Complex Systems in 2008 he has been pursuing the use of Data
Science and Machine Learning to study Human Behavior. Using large datasets
from Twitter, Wikipedia, web access logs, and Yahoo! Meme he studied how we
can observe both large scale and individual human behavior in an obtrusive
and widespread manner. The main applications have been to the study of
Computational Linguistics, Information Diffusion, Behavioral Change and
Epidemic Spreading. In 2015 he was awarded the Complex Systems Society's
2015 Junior Scientific Award for "outstanding contributions in Complex
Systems Science" and in 2018 is was named a Science Fellow of the Institute
for Scientific Interchange in Turin, Italy.
