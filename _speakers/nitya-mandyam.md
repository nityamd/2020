---
name: Nitya Mandyam
talks:
- "A Monte Carlo Murder Mystery: A Holmes-Watson-PyMC3 adventure!"
---
Nitya is an astronomer turned data scientist and a NYC PyLadies organizer
with a passion for problem-solving, teaching and inclusivity in tech.
