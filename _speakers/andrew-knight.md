---
name: Andrew Knight
talks:
- "Decorators Are Great! Act Now!!"
---
Andy Knight is the “Automation Panda” - an engineer, consultant, and
Pythonista who loves all things software. He specializes in building robust
test automation solutions from the ground up. He currently works at
PrecisionLender in Cary, NC. Read his tech blog at AutomationPanda.com, and
follow him on Twitter at @AutomationPanda.
