---
name: Tadeh Hakopian
talks:
- "Python for Skyscrapers - Building Blocks of Code"
---
Tadeh is a developer and designer in Architecture (buildings not computers).
He has been a course author, trainer and open source contributor. Over the
years he has taught other designers the value of coding and automation and
wants to continue to spread that message to as many people as possible with
training seminars and talks. He has been a speaker at national conferences
about design, tech, Architecture and loves to talk about new possibilities
with innovation and technology.
