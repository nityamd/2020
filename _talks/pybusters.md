---
duration:
presentation_url: null
room:
slot:
speakers:
- Aaron Bassett
- Joe Drumgoole
- Mark Smith
title: PyBusters
type: talk
video_url: null
---

Step back into the Golden Age of television game shows with us as we put on
a very special Python themed version of
[Blockbusters](https://www.youtube.com/watch?v=vddbWs8TOLk)
([UK](https://www.youtube.com/watch?v=c5m9luH0Da4)). Watch as Pythonistas
compete in a knockout tournament to determine who will be the 2020 PyGotham
TV PyBuster!
