---
duration: 30
presentation_url: null
room: Online
slot: 2020-10-03 15:50:00-04:00
speakers:
- Jay Miller
title: Getting Better Transcriptions (for Developers)
type: talk
video_url: null
---

Transcriptions seem like an often forgotten standard in making your multimedia
accessible. Manual services are expensive and automated services are inaccurate.

This talk looks at how to implement some transcription standards. It also
showcases _Transcriptor_, a wrapper around Amazon Transcribe, designed to make
transcriptions easier to work with (especially for developers).
