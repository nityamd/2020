---
duration:
presentation_url: null
room:
slot:
speakers:
- April Slotterback
title: Serializing Boiler Repair Work With Marshmallow
type: talk
video_url: null
---

When a boiler leaks in a New York City high rise building, service
technicians must strike a delicate balance between restoring boiler
operation as quickly as possible, and leaving the boiler offline long enough
to minimize risk of injury to repair crews. Once repairs are complete, the
lead service tech has a brief window in which to precisely build a report on
the extent of repairs before the boiler is bolted shut. I will show how I
use Marshmallow within a custom boiler repair reporting app to serialize
custom form inputs for entry into a common service database so that service
managers can act on timely repair data.
