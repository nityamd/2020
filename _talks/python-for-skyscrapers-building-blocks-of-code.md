---
duration: 16
presentation_url: null
room: Online
slot: 2020-10-03 11:30:00-04:00
speakers:
- Tadeh Hakopian
title: Python for Skyscrapers - Building Blocks of Code
type: talk
video_url: null
---

When I started working on the latest high rise for San Francisco a challenge
stood before me: How am I going to do this without losing my mind?
Thankfully coding came to the rescue and showed me what is possible. Now

This talk explains how I used Python to dive into open source tools for my
projects. This includes scripting with open source tools like Dynamo, Rhino
and Blender. My talk is meant to encourage others to learn how to use open
source tools with Python and enhance their projects whether it is in digital
or physical space.
