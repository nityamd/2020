---
duration: 15
presentation_url: null
room: Online
slot: 2020-10-03 10:45:00-04:00
speakers:
- Shamil Turner
title: Converting Powershell to Python, an onboarding story
type: talk
video_url: null
---

Admins recently have been tasked with adapting internal practices to a new
world of remote work.  As part of that adoption of SaaS cloud first products
has exploded in their organizations.  Windows administrators who have
traditionally leveraged Powershell to manage their internal applications are
now left with a gap in coverage for these SaaS applications due to a lack of
Powershell modules available to automate tasks in these applications.  With
a little bit of work, admins can quickly transform their scripts to leverage
Python and take advantage of the available libraries purpose built for these
SaaS applications.

Join Shamil to step through available Python libraries for Slack and
Dropbox, and help automate administrative tasks in these platforms.
