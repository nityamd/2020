---
duration: 25
presentation_url: null
room: Online
slot: 2020-10-02 16:40:00-04:00
speakers:
- Piper Thunstrom
title: Cooking Up Easy to Publish Games with a Bear and a Briefcase
type: talk
video_url: null
---

One of the biggest challenges for games in Python is freezing and shipping
to end users. Join Piper Thunstrom as she demonstrates two great recipes to
deliver your video game to your users with no pain. Get a feel for
[Beeware's Briefcase](https://beeware.org/project/projects/tools/briefcase/)
to set up our project and freeze your end user application, and see the ease
of use of [ppb](https://ppb.dev) for building your own games.

Make real games that can really be installed on end users computers using
pure Python.
