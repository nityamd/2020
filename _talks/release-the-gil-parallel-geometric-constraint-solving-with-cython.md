---
duration: 20
presentation_url: null
room: Online
slot: 2020-10-02 13:30:00-04:00
speakers:
- Maryanne Wachter
title: Release the GIL! Parallel Geometric Constraint Solving with Cython
type: talk
video_url: null
---

One of the common complaints about Python is that while it is approachable,
it is impossible to match the performance of a compiled language such as C
or C++. However, using Cython allows you to have the best of both worlds.
You can preserve the structure and readability of a Python package with the
performance of a C/C++ program. Common pitfalls of Cython will be discussed
through the lense of adapting an existing geometric constraint solving C++
library used widely in both architectural design and structural engineering.
During this talk, particular emphasis will be placed on how this project was
structured to allow for releasing the GIL to parallelize specific solve
methods for a 100x speedup in simulation runtimes compared to pure Python.

The goal of this talk is to show how Cython can be used for implementing
powerful algorithms in Python to develop code that is approachable and
extensible.
